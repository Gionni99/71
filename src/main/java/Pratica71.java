
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    public static void main(String[] args) throws IOException {
        Scanner tucano = new Scanner(System.in);
        Time Palmera;
        Palmera = new Time();
        int njogadores;
        System.out.print("Quantos jogadores?");
        njogadores=tucano.nextInt();
        for(int i=0;i<njogadores;i++){
            String proplayer;
            int romero;
            System.out.println("Qual o número do jogadô?");
            romero=tucano.nextInt();
            System.out.println("E o nome dele? Calé?");
            proplayer=tucano.next();
            Jogador oniichan = new Jogador(romero,proplayer);
            System.out.println("E calé a posição do minino prodígio?");
            String posicao=tucano.next();
            Palmera.addJogador(posicao,oniichan);
        }
        JogadorComparator o=new JogadorComparator(true,true,true);
        List<Jogador> listinha;
        listinha=Palmera.ordena(o);
        System.out.println("Escalacao:"+Palmera.getJogadores().toString());
        int numero=1;
        while(numero!=0){
            String proplayer;
            System.out.println("Qual o número do jogadô?");
            numero=tucano.nextInt();
            System.out.println("E o nome dele? Calé?");
            proplayer=tucano.next();
            Jogador oniichan = new Jogador(numero,proplayer);
            System.out.println("E calé a posição do minino prodígio?");
            String posicao=tucano.next();
            Palmera.addJogador(posicao,oniichan);
        }
    }
}
